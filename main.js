const { Country } = require('./Country')
const util = require('util')

let country = new Country('Ireland')
let cities = ['Dublin', 'Bray', 'Limerick', 'Dundalk', 'Wexford',
              'Galway','Cork','Wateford','Kilkenny','Newbridge','Naas']
country.addCapital('Dublin')
country.cities[0].setAirport('Dublin Airport')

for (let city = 1; city < cities.length; city++) {
    country.addCity(cities[city])
}

async function processArray(array) {
    for (city in array) {
        await country.cities[city].setWeather(country.cities[city]).then(() => {
        }).catch((error) => {
            console.log(error)
        })
    }
    for (city in array) {
        await country.cities[city].setForecast(country.cities[city]).then(() => {
        }).catch((error) => {
            console.log(error)
        })
    }
}


processArray(cities).then(() => {
    country.sortByTemp('desc')
}).then(() => {
    console.log(util.inspect(country, false, null, true))
})

//order can be set to ascending or descending by passing either 'asc' or 'desc'

console.log('Waiting for response...')

