const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather{
    /**
     * Describe attributes: temperature, wind, date
     */
    constructor() {
        this.temperature = ''
        this.wind = ''
        let todayDate = new Date(Date.now())
        this.date = todayDate.toUTCString()
        this.forecast = []
    }

    async setWeather(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
            this.forecast = []
        }).catch((error) => {return error.message})
    }

    async setForecast(cityName) {
        await axios.get(weatherUrl + cityName).then((response) => {
            
            this.forecast = response.data.forecast

        }).catch((error) => { return error.message })
    }
}

/*let weather = new Weather()
weather.setWeather('Kyiv').then(() => {
    console.log(weather)
}).catch((error) => {
    console.log(error)
})
weather.setForecast('Kyiv').then(() => {
    console.log(weather)
}).catch((error) => {
    console.log(error)
})*/


module.exports = {Weather}