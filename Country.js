const { City } = require('./City')
const { Capital } = require('./Capital')
class Country {
    constructor(name) {
        this.countryName = name
        this.cities = []
    }

    addCity(name) {
        let city = new City(name)
        this.cities.push(city)
        return city
    }

    addCapital(name) {
        let capital = new Capital(name)
        this.cities.push(capital)
        return capital
    }

    getAllCities() {
        let array = []

        for (city in this.cities) {
            array.push(this.cities[city].name)
        }

        return array
    }

    deleteCity(name) {
        let index = this.cities.indexOf(name)
        this.cities.splice(index, 1)
        console.log(`${name} was deleted`)
    }

    sortByTemp(order) {
        let unsortedCities = this.cities.slice(0)

        if (order = 'desc') {
            unsortedCities.sort(function (a, b) {
                return parseInt(b.weather.temperature) - parseInt(a.weather.temperature)
            })
        }

        else if (order = 'asc') {
            let unsortedCities = this.cities.slice(0)
            unsortedCities.sort(function (a, b) {
                return parseInt(a.weather.temperature) - parseInt(b.weather.temperature)
            })
        }

        this.cities = unsortedCities

    }

}

module.exports = { Country }


